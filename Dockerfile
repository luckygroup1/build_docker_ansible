FROM alpine

RUN apk update
RUN apk --no-cache add ansible
RUN apk add openssh
